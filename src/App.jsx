import { Component } from 'react';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';
import Modal from './components/Modal/Modal';

class App extends Component {

    state = {
        goods: [],
        isModalVisible: false,
        cart: [],
        pickedOut: [],
        active: {}
    }

    modal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    changeActiveProduct = (productObj) => {
        this.setState({
            active: productObj
        });
    }

    addToCart = (productObj) => {
        this.setState((prev) => ({ cart: [...prev.cart, productObj] }), () => {
            localStorage.setItem('cart', JSON.stringify((this.state.cart)));
        })
    }

    addToPickedOut = (productObj) => {
        if (!this.state.pickedOut.find(e => e.productCode === productObj.productCode)) {
            this.setState((prev) => ({ pickedOut: [...prev.pickedOut, productObj] }), () => {
                localStorage.setItem('pickedOut', JSON.stringify((this.state.pickedOut)));
            })
        }
    }

    componentDidMount() {

        const cart = JSON.parse(localStorage.getItem('cart'));
        const pickedOut = JSON.parse(localStorage.getItem('pickedOut'));

        cart ? this.setState({ cart: cart }) : localStorage.setItem('cart', '[]');
        pickedOut ? this.setState({ pickedOut: pickedOut }) : localStorage.setItem('pickedOut', '[]');

        fetch('products.json')
            .then(res => res.json())
            .then(response => this.setState({
                goods: response
            }))
    }

    render() {
        return (
            <>
                <Header cart={this.state.cart} pickedOut={this.state.pickedOut} />

                <Main goods={this.state.goods} modal={this.modal} addToPickedOut={this.addToPickedOut} changeActiveProduct={this.changeActiveProduct} />

                <Footer />

                <Modal header="Add this product to cart?" text="You can add this product to cart or undo adding by closing the window." visible={this.state.isModalVisible} hideModal={this.modal} changeActiveProduct={this.changeActiveProduct} addToCart={this.addToCart} activeProduct={this.state.active} />

            </>
        );
    }
}
export default App;

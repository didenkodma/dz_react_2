import { Component } from 'react';
import "./Main.scss";
import CarouselSection from '../CarouselSection';
import WelcomeSection from '../WelcomeSection';
import ProductsSection from '../ProductsSection';
import PartnersSection from '../PartnersSection';
import PropTypes from 'prop-types';

class Main extends Component {
    render() {
        return (
            <main className='main'>

            <CarouselSection />

            <WelcomeSection />

            <ProductsSection goods={this.props.goods} showModal={this.props.modal} addToPickedOut={this.props.addToPickedOut} changeActiveProduct={this.props.changeActiveProduct} />

            <PartnersSection />

            </main>
        );
    }
}

Main.propTypes = {
    goods: PropTypes.arrayOf(PropTypes.object).isRequired,
    modal: PropTypes.func.isRequired,
    addToPickedOut: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired
}


export default Main;
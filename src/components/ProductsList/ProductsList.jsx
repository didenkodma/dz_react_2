import { Component } from 'react';
import "./ProductsList.scss";
import Product from '../Product';
import PropTypes from 'prop-types';

class ProductsList extends Component {
    render() {
        const { goods, showModal, addToPickedOut, changeActiveProduct } = this.props;
        return (
            <div className='products__list'>
                {goods.map( element => {
                    const {color, imgSrc, name, price, productCode, text} = element;
                    return <Product key={productCode} color={color} imgSrc={imgSrc} name={name} price={price} productCode={productCode} text={text} showModal={showModal} addToPickedOut={addToPickedOut} changeActiveProduct={changeActiveProduct} />
                })}
            </div>
        );
    }
}

ProductsList.propTypes = {
    goods: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    addToPickedOut: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired
}

export default ProductsList;
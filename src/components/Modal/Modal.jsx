import { Component } from 'react';
import './Modal.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

class Modal extends Component {

    clickCloseHandler = () => {
        const {hideModal, changeActiveProduct} = this.props;
        hideModal();
        changeActiveProduct({});
    }

    clickAddHandler = () => {
        const {hideModal, changeActiveProduct, activeProduct, addToCart} = this.props;
        addToCart(activeProduct);
        hideModal();
        changeActiveProduct({});
    }

    render() {
        const { header, text, closeButton, visible } = this.props;
        return visible ? (
            <div className='modal' onClick={this.clickCloseHandler}>
                <div className='modal-content' onClick={(e) => {
                    e.stopPropagation();
                }}>
                    <div className='modal-top'>
                        <h2 className='modal-header'>{header}</h2>
                    </div>
                    <div className='modal-bottom'>
                        <p className='modal-text'>{text}</p>
                        <div className='modal-button-container'>

                            <Button backgroundColor="#1e1e20" padding="1.3rem 3.3rem" text='Add' onClick={this.clickAddHandler} />

                            <Button backgroundColor="#1e1e20" padding="1.3rem 2.7rem" text='Close' onClick={this.clickCloseHandler} />

                        </div>
                    </div>
                    {closeButton && <button className='modal-close-btn' onClick={this.clickCloseHandler}></button>}
                </div>
            </div>
        ) : null;
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    closeButton: PropTypes.bool,
    visible: PropTypes.bool.isRequired,
    hideModal: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired,
    addToCart: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
    activeProduct: PropTypes.object.isRequired
}

Modal.defaultProps = {
    header: "Modal",
    text: 'Some modal dummy text',
    closeButton: false,
    addToCart: false
}

export default Modal;
import { Component } from 'react';
import "./ProductsSection.scss";
import ProductsList from '../ProductsList';
import PropTypes from 'prop-types';

class ProductsSection extends Component {
    render() {
        return (
            <section className='products'>

                <div className='container'>

                    <div className='products__content'>
                        <h2>Albums Currently on sale</h2>

                        <ProductsList goods={this.props.goods} showModal={this.props.showModal} addToPickedOut={this.props.addToPickedOut} changeActiveProduct={this.props.changeActiveProduct} />
                    </div>
                </div>

            </section>
        );
    }
}

ProductsSection.propTypes = {
    goods: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    addToPickedOut: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired
}

export default ProductsSection;
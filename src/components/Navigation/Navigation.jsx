import { Component } from 'react';
import "./Navigation.scss";


class Navigation extends Component {

    render() {
        return (
            <nav className='navigation'>
                <ul className='navigation__list'>
                    <li className='navigation__item'><a className='navigation__link' href="#">Home</a></li>
                    <li className='navigation__item'><a className='navigation__link' href="#">Cd'<span>s</span></a></li>
                    <li className='navigation__item'><a className='navigation__link' href="#">Dvd'<span>s</span></a></li>
                    <li className='navigation__item'><a className='navigation__link' href="#">News</a></li>
                    <li className='navigation__item'><a className='navigation__link' href="#">Portfolio</a></li>
                    <li className='navigation__item'><a className='navigation__link' href="#">Contact us</a></li>
                </ul>
                <button className="navigation__button">
                    <span className="navigation__icon">&nbsp;</span>
                </button>
            </nav>
        );
    }
}
export default Navigation;
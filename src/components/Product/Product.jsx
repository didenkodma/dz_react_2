import { Component } from 'react';
import "./Product.scss";
import Button from '../Button/Button';
import Icon from '../Icon';
import PropTypes from 'prop-types';

class Product extends Component {

    state = {
        isPickedOut: false
    }

    clickPickedOutHandler = () => {
        const { color, imgSrc, name, price, productCode, text, addToPickedOut } = this.props;
        const activeProduct = {color, imgSrc, name, price, productCode, text};
        this.setState( {isPickedOut: true});
        addToPickedOut(activeProduct);
    }

    clickAddToCartHandler = () => {
        const { color, imgSrc, name, price, productCode, text, showModal, changeActiveProduct } = this.props;
        const activeProduct = {color, imgSrc, name, price, productCode, text};
        showModal();
        changeActiveProduct(activeProduct);
    }

    componentDidMount() {
        const pickedOutProducts = JSON.parse(localStorage.getItem('pickedOut'));
        if (pickedOutProducts.find(e => e.productCode === this.props.productCode)) {
            this.setState({
                isPickedOut: true
            })
        }
    }

    render() {
        const { color, imgSrc, name, price, productCode, text } = this.props;

        const priceCustomStyle = {
            color: color,
        }

        return (
            <div className='product'>
                <img className='product__img' src={imgSrc} alt={`Product ${productCode}`} />
                <div className='product__content'>
                    <h4 className='product__heading'>{name}</h4>
                    <p className='product__text'>{text}</p>
                    <div className='product__add'>
                        <span className='product__price' style={priceCustomStyle}>{price}</span>
                        <Button backgroundColor="#1e1e20" padding=".9rem .5rem" text='Add to cart' onClick={this.clickAddToCartHandler} />
                    </div>
                    <button className='product__picked-out' onClick={this.clickPickedOutHandler}>
                        {this.state.isPickedOut
                            ?
                            <Icon color="#cc3333" type="star-fill" />
                            :
                            <Icon color="#cc3333" type="star" />
                        }
                    </button>
                </div>
            </div>
        );
    }
}

Product.propTypes = {
    color: PropTypes.string,
    imgSrc: PropTypes.string.isRequired,
    name: PropTypes.string,
    price: PropTypes.string,
    productCode: PropTypes.string.isRequired,
    text: PropTypes.string,
    showModal: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired
}

Product.defaultProps = {
    color: '#1e1e20',
    name: 'Some product on sale',
    price: '$0.00',
    text: 'Some dummy product text'
}

export default Product;
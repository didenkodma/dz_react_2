import { Component } from 'react';
import "./FooterBottom.scss";

class FooterBottom extends Component {
    render() {
        return (
            <div className='footer-bottom'>
                <div className='footer-bottom'>

                    <div className='container'>

                        <div className='footer-bottom__content'>

                            <nav className='footer-bottom__navigation'>
                                <ul className='footer-bottom__navigation-list'>
                                    <li className='footer-bottom__navigation-item'><a className='footer-bottom__navigation-link' href="#">Home</a></li>
                                    <li className='footer-bottom__navigation-item'><a className='footer-bottom__navigation-link' href="#">Protfolio</a></li>
                                    <li className='footer-bottom__navigation-item'><a className='footer-bottom__navigation-link' href="#">Sitemap</a></li>
                                    <li className='footer-bottom__navigation-item'><a className='footer-bottom__navigation-link' href="#">Contact</a></li>
                                </ul>
                            </nav>

                            <p className='footer-bottom__copyright'>Musica &copy;2022 developed by didenko.dma | All Rights Reserved</p>

                        </div>

                    </div>



                </div>
            </div>
        );
    }
}
export default FooterBottom;
import { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import "./CarouselSection.scss";
import { Carousel } from 'react-responsive-carousel';

class CarouselSection extends Component {
    render() {
        return (
            <section className='carousel-section'>
                <Carousel showThumbs={false} showIndicators={false} showStatus={false} dynamicHeight={true} autoPlay={true} infiniteLoop={true}>
                    <div>
                        <img src="img/slider-img.png" alt="img 1" />
                    </div>
                    <div>
                        <img src="img/slider-img.png" alt="img 2" />
                    </div>
                    <div>
                        <img src="img/slider-img.png" alt="img 3" />
                    </div>
                </Carousel>
            </section>

        );
    }
}

export default CarouselSection;
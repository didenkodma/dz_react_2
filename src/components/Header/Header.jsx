import { Component } from 'react';
import "./Header.scss";
import HeaderTop from '../HeaderTop';
import HeaderBottom from '../HeaderBottom';
import PropTypes from 'prop-types';

class Header extends Component {
    render() {
        return (
            <header className='header' id="header">

                <HeaderTop cart={this.props.cart} pickedOut={this.props.pickedOut} />

                <HeaderBottom />    

            </header>
        );
    }
}

Header.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    pickedOut: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Header;
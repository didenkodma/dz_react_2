import { Component } from 'react';
import "./HeaderTop.scss";
import Cart from '../Cart/Cart';
import PickedOut from '../PickedOut';
import PropTypes from 'prop-types';

class HeaderTop extends Component {
    render() {
        return (
            <div className='header-top'>

                <div className='container'>
                    <div className='header-top__icons-bar'>
                        <button className='header-top__icon-btn'><i className="fa fa-brands fa-facebook"></i></button>
                        <button className='header-top__icon-btn'><i className="fa fa-solid fa-basketball-ball"></i></button>
                        <button className='header-top__icon-btn'><i className="fa fa-brands fa-twitter"></i></button>
                        <button className='header-top__icon-btn'><i className="fa fa-solid fa-envelope"></i></button>
                        <button className='header-top__icon-btn'><i className="fa fa-brands fa-vimeo"></i></button> 
                    </div>
                    <div className='header-top__user-tools'>

                        <PickedOut pickedOut={this.props.pickedOut} />

                        <Cart cart={this.props.cart} />
                        
                    </div>
                </div>

            </div>
        );
    }
}

HeaderTop.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    pickedOut: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default HeaderTop;
import { Component } from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends Component {

    clickHandler = (e) => {
        this.props.onClick(e.target.innerText)
    }

    render() {
        const {backgroundColor, padding, text } = this.props;

        const buttonCustomStyle = {
            backgroundColor: backgroundColor,
            padding: padding
        }

        return (
            <button className='button' style={buttonCustomStyle} onClick={this.clickHandler}>{text}</button>
        );
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string.isRequired,
    padding: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string
}

Button.defaultProps = {
    text: 'Button'
}


export default Button;
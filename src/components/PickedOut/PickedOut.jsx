import { Component } from 'react';
import "./PickedOut.scss";
import Icon from '../Icon';
import PropTypes from 'prop-types';

class PickedOut extends Component {
    render() {
        return (
            <button className='picked-out'>
                
                <Icon color="#fff" type="star-fill" />
                <span>({this.props.pickedOut.length})</span>
            </button>
        );
    }
}

PickedOut.propTypes = {
    pickedOut: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default PickedOut;
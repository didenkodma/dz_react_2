import { Component } from 'react';
import "./Cart.scss";
import Icon from '../Icon';
import PropTypes from 'prop-types';

class Cart extends Component {
    render() {
        return (
            <button className='cart'>
                
                <Icon color="#fff" type="cart" />
                <span>({this.props.cart.length})</span>
            </button>
        );
    }
}

Cart.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Cart;